function p = interpol(y, t, theta)
    //1 : call diffdiv function to calculate the coefficient a
    a = diffdiv(y, t)
    
    //2 : call horn to calculate de p
    n = size(t)(1)
    p = horn(a, t(1:n-1), theta)(1)
endfunction
