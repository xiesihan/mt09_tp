//Préparation
exec('diffdiv.sci', -1)
t = [0;10;20]
y = [100;200;300]
f = diffdiv(y, t)

//EX1
t1 = [1; 3; 4.5; 5; 6]
y1 = [1; 5; 3; 7; -1]
f1 = diffdiv(y1, t1)

//EX2
//a
//https://web.physics.utah.edu/~detar/lessons/c++/array/node4.html
exec('horn.sci', -1)
a = [1;3;5;-1]
t = [0;0;1]
theta = 2
[p, pprim] = horn(a, t, theta)

//b
exec('hornv.sci', -1)
thetas = [2 ; 4]
ps = hornv(a, t , thetas)

//EX3
//a
exec('interpol.sci', -1)
t = [1; 3; 4.5; 5; 6]
y = [1; 5; 3; 7; -1]
theta1 = 3
theta2 = 4
p1 = interpol(y, t, theta1) //5
p2 = interpol(y, t, theta2) //0.0809524

//b
//a
/*
exec('courbe.sci', -1)
t = [1; 3; 4.5; 5; 6]
y = [1; 5; 3; 7; -1]
tt = 4
N=100
courbe(y, t, N, tt)
*/

//EX4

exec('solinf.sci', -1)
exec('solsup.sci', -1)
exec('cholesky.sci', -1)
exec('resolchol.sci', -1)
exec('place.sci', -1)
exec('calcg.sci', -1)
exec('cald.sci', -1)
exec('calcoef.sci', -1)
exec('courbebis.sci', -1)
//courbebis(y, t, 100)


//case 2
scf(1)
t1 = linspace(0, 1, 11);
y1 = zeros(11,1)
y1(6) = 10
courbebis(y1, t1', 100)




