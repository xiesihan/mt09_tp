function courbe(y, t, N, tt)
    //theta in range [min(t), max(t)]
    thetas = linspace(min(t), max(t), N);
    m = size(thetas)(2)
    p = zeros(1,m)
    for j=1:m
        p(1,j) = interpol(y, t, thetas(1,j))
    end
    title("EXO 3b : Polynome d’interpolation")
    plot(thetas,p)
    plot(t, y ,'ro')
    //the line tangent to the curve at the abscissa point tt. 
    a = diffdiv(y, t)
    n = size(t)(1)
    [p_tt,pprim_tt] = horn(a, t(1:n-1), tt)
    b=p_tt-pprim_tt*tt
    tts = zeros(1,m)
    for j=1:m
        tts(1,j) = pprim_tt*thetas(1,j)+b
    end
    plot(thetas,tts,'g.')
    
endfunction
