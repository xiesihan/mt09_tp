function z = mcnormbis(A, y)
    z = resolchol((A')*A,(A')*y)
endfunction
