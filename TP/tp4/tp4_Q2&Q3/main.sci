exec('courbe.sci', -1)
exec('foncjac.sci', -1)
exec('newton.sci', -1)

//Exo2
courbe(100)

//test case 1 : Point de départ (2,0)
[x,k]=newton(foncjac,1E-6,100,[2;0])
plot(x(:,1),x(:,2),'ro')
title("EXO 2 : Point de départ (2,0)")

//test case 2 : 
scf(1)//nouvelle fenetre
courbe(100)
[x,k]=newton(foncjac,1E-6,100,[-2;1])
plot(x(:,1),x(:,2),'ro')
title("EXO 2 : Point de départ (-2,1)")





//Exo3
//définir les fonctions b, g et g_derive
deff("b=b(x)","b=-x*(x-1)")
deff("g=g(x)","g=10*x/(1+x)")
deff("g_derive=g_derive(x)","g_derive=10/((1+x)^2)")

//définir les params alpha,beta et n
alp = 5
bet = 5
n1 = 10
n2 = 20

//point de départ v1 et v2 pour n=10 et n=20
v1 = [0;0;0;0;0;0;0;0;0]
v2 = [0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0]

exec('foncjac2.sci', -1)
exec('newton2.sci', -1)

//test case 1 : n = 10
[v1_res, k1] = newton2(foncjac2,1E-6, 100, v1,n1)
//test case 2 : n = 20
[v2_res, k2] = newton2(foncjac2,1E-6, 100, v2,n2)
//les tracer sur une meme figure
X1 = zeros(n1+1,1)
V1 = zeros(n1+1,1)
X2 = zeros(n2+1,1)
V2 = zeros(n2+1,1)
h1 = 1/n1
h2 = 1/n2

//X1
for i = 1 : n1
    X1(i+1) = i*h1
end

//X2
for i = 1 : n2
    X2(i+1) = i*h2
end

//V1
V1(1)=alp
for i = 2 : n1
    V1(i) = v1_res(i-1)
end
V1(n1+1)=bet

//V2
V2(1)=alp
for i = 2 : n2
    V2(i) = v2_res(i-1)
end
V2(n2+1)=bet

scf(2)//nouvelle fenetre
plot(X1,V1,'r')
plot(X2,V2,'b')
title("EXO 3 : n=10 en rouge, n=20 en bleu")

