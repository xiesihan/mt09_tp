function Z =RK4 (a,t0,T,N,f)
    h = T/N
    p = size(a)(1)
    Z = zeros(p, N+1)
    Z(:,1) = a
    for n=1:N
        K0 = f(t0+n*h, Z(:,n))
        K1 = f(t0+n*h+h/2, Z(:,n)+(h/2)*K0)
        K2 = f(t0+n*h+h/2, Z(:,n)+(h/2)*K1)
        K3 = f(t0+n*h+h, Z(:,n)+h*K2)
        Z(:,n+1)=Z(:,n)+ (h/6)*(K0+2*K1+2*K2+K3)
    end
endfunction
