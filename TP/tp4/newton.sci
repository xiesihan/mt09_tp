function [x, k] = newton(foncjac, tol, Kmax, x0)
    //vérifier si x0 est déja la solution
    [F,J] = foncjac(x0)
    if (norm(F)<tol) 
        [x, k]=resume(x0, 0)
    else
        xs = zeros(1, 2); 
        xs(1,1)=x0(1,1)
        xs(1,2)=x0(2,1)
        x=x0
        for k = 0 : Kmax
            [F,J] = foncjac(x)
            h = J\(-F)
            x = x + h
            xs(k+2,1)=x(1,1)
            xs(k+2,2)=x(2,1)
            if (norm(F)<tol) then
                break
            end 
        end
        if k==Kmax 
            disp("Newton did not converge : Reached maximum number of iterations ...")
        else
            x=xs
            
        end
    end
endfunction
