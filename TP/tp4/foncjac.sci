function [F,J]=foncjac(x) //x : vecteur, F: vecteur, J : Matrice Jacobienne
    //Calculer F
    F(1,1) = (x(1,1)^2+x(2,1)^2)-4
    F(2,1) = x(2,1)-exp(x(1,1))
    //Calculer J
    J(1,1) = 2*x(1,1)
    J(1,2) =2*x(2,1)
    J(2,1) = -exp(x(1,1))
    J(2,2) = 1
endfunction
