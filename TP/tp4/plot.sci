//Plot the circle
//center of the circle
xc = 1;
yc = 0;

//radius of the circle
r = 2;

//dummy variable for angle in range [0, 2*%pi]
a = linspace(0, 2*%pi, 100);

//x axis
x = xc + r*cos(a);

//y axis
y = yc + r*sin(a);

//plot the circle
plot(x, y);


//Plot the hyperbole
a = linspace(-1,2, 100);
plot(x,x^2)
