function [z]=calcg(theta, t, cc)
    n = size(t)(1)
    [mcc,ncc] = size(cc)
    if(n~=mcc+1 | ncc~=4 ) then
        error("Taille non correcte");
    end 
    if(theta==t(n,1))
        T = zeros(3,1)
        T(1) = t(n-1)
        T(2) = t(n-1)
        T(3) = t(n)
        z = horn(cc(n-1,:)',T,theta)(1)
    else
        i = place(t,theta)
        T = zeros(3,1)
        T(1) = t(i)
        T(2) = t(i)
        T(3) = t(i+1)
        z = horn(cc(i,:)',T,theta)(1)
    end
endfunction
