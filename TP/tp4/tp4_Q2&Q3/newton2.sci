function [v, k] = newton2(foncjac2, tol, Kmax, v0,n)
    //vérifier si x0 est déja la solution
    [F,J] = foncjac2(v0,n)
    if (norm(F)<tol) 
        [v, k]=resume(x0, 0)
    else
        v=v0
        for k = 0 : Kmax
            [F,J] = foncjac2(v, n)
            h = J\(-F)
            v = v + h
        
            if (norm(F)<tol) then
                break
            end 
        end
        
        if k==Kmax 
            disp("Newton did not converge : Reached maximum number of iterations ...")
        end
    end
endfunction
