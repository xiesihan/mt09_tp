function [p, pprim] = horn(a, t, theta)
    n = size(a)(1)
    p = a(n)
    pprim = 0
    for k = n-1 : -1 :1
        pprim = pprim * (theta-t(k)) + p
        p = a(k)+(theta-t(k))*p
    end
endfunction
