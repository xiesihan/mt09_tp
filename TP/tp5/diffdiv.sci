function [f] = diffdiv(y, t)
    n = size(y)(1)
    m = size(t)(1)
    if n~=m  then
        error("Taille incorrecte");
    end
    
    //init f
    f = y
    
    for k = 2 : n
        for i = n : -1 : k 
            f(i) = (f(i)-f(i-1))/(t(i)-t(i-k+1))
        end
    end
endfunction
