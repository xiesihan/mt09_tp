function courbebis(y, t, N)
    //P
    //theta in range [min(t), max(t)]
    thetas = linspace(min(t), max(t), N);
    m = size(thetas)(2)
    p = zeros(1,m)
    for j=1:m
        p(1,j) = interpol(y, t, thetas(1,j))
    end
    title("EXO 3b : Polynome d’interpolation")
    //plot(thetas,p,'k')
    //plot(t, y ,'ro')
    scf(1)
    //G
    cc = calcoef(t,y)
    z = zeros(1,m)
    for j=1:m
        z(1,j) = calcg(thetas(1,j), t, cc)
    end
    plot(thetas,z,'r')
endfunction
