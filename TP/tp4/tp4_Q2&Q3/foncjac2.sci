function [F,J]=foncjac2(v,n)
    //Calculer F
    h=1/n
    m = size(v)(1)
    F=zeros(m, 1); 
    F(1) = -alp+2*v(1)+h*h*g(v(1))-v(2)-h*h*b(h)
    F(m) = -v(m-1)+2*v(m)+h*h*g(v(m))-bet-h*h*b(m*h)
    for i = 2 : m-1
        F(i)=-v(i-1)+2*v(i)+h*h*g(v(i))-v(i+1)-h*h*b(i*h)        
    end
    
    
    //calculer A
    A= zeros(m,m)
    A(1,1)=2
    A(1,2)=-1
    A(m,m)=2
    A(m,m-1)=-1
    for i = 2 : m-1
        A(i,i)=2
        A(i,i-1)=-1
        A(i,i+1)=-1
    end
    
    //calculer Gderive
    Gderive= zeros(m,m)
    for i = 1 : m
        Gderive(i,i)=g_derive(v(i))
    end
    //Calculer J
    J=A+h*h*Gderive
 
endfunction
