function [F,J]=foncjac2(v,alpha,bet,n)
    //Calculer F
    h=1/n
    [m,n] = size(v)
    F=zeros(m, 1); 
    F(1,1) = -alpha+2*v(1,1)+h*h*g(v(1,1))-v(2,1)-h*h*b(h)
    F(m,1) = -v(m-2,1)+2*v(m-1,1)+h*h*g(v(m-1,1))-bet-h*h*b((m-1)*h)
    for i = 2 : n-2
        F(i,1)=-v(i-1,1)+2*v(i,1)+h*h*g(v(i,1))-v(i+1,1)-h*h*b(i*h)        
    end
    
    
    //calculer A
    A= zeros(m,m)
    A(1,1)=2
    A(1,2)=-1
    A(m,m)=2
    A(m,m-1)=-1
    for i = 2 : m-1
        A(i,i)=2
        A(i,i-1)=-1
        A(i,i+1)=-1
    end
    
    //calculer gder
    Gder= zeros(m,m)
    for i = 1 : m
        Gder(i,i)=gder(v(i,1))
    end
    //Calculer J
    J=A+h*h*Gder
    
    
endfunction
