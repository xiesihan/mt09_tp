function [x, k] = newton2(foncjac2, tol, Kmax, x0,n)
    //vérifier si x0 est déja la solution
    [F,J] = foncjac2(x0,5,5,n)
    if (norm(F)<tol) 
        [x, k]=resume(x0, 0)
    else
        [m,n]=size(F)
        xs = zeros(1, m); 
        
        for j = 1:m
            xs(1,j)=x0(j,1)
        end
        
        x=x0
        for k = 0 : Kmax
            [F,J] = foncjac2(x,5,5,n)
            h = J\(-F)
            x = x + h
            
            for j = 1:m
                xs(k+2,j)=x(j,1)
            end
           
            if (norm(F)<tol) then
                break
            end 
        end
        if k==Kmax 
            disp("Newton did not converge : Reached maximum number of iterations ...")
        else
            x=xs
        end
    end
endfunction
