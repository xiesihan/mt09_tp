//Préparartion
exec "eulerexpl.sci"
exec "funexpt2.sci"
a = 0
t0 = 0
T = 5
N = 10

Z = eulerexpl(a, t0, T, N, funexpt2)
t=linspace(0,5,N+1)
y= 2*exp(t)-(t^2)-(2*t)-2

//N = 10
plot(t,Z,".b")
plot(t,y,"r")
title("Préparation : N = 10")


//N = 20
N1 = 20
Z1 = eulerexpl(a, t0, T, N1, funexpt2)
t1=linspace(0,5,N1+1)
y1= 2*exp(t1)-(t1^2)-(2*t1)-2
scf(1)
plot(t1,Z1,".b")
plot(t1,y1,"r")
title("Préparation : N = 20")


//N = 50
N2 = 50
Z2 = eulerexpl(a, t0, T, N2, funexpt2)
t2=linspace(0,5,N2+1)
y2= 2*exp(t2)-(t2^2)-(2*t2)-2
scf(2)
plot(t2,Z2,".b")
plot(t2,y2,"r")
title("Préparation : N = 50")


//TP7
exec "f.sci"
exec "pointmilieu.sci"
exec "vdp.sci"
exec "RK4.sci"
exec "tracevdp.sci"
exec "compar.sci"

//Exo 1&2&3&4
a = [2;-2]
t0 = 0
T =15
N = 100
tracevdp(a,t0,T, N,N,N,N)

//Exo 5
//a&b&c
[TV,TE] = compar(1,0,10)
//d
err_euler=-log10(TE(:,1))'
err_pm=-log10(TE(:,2))'
err_RK4=-log10(TE(:,3))'
N=[10,100,1000,10000]
scf(7)
plot(log10(N),err_euler,"r")    
plot(log10(N),err_pm,"g")
plot(log10(N),err_RK4,"b")
title("-log10(Erreur) en fonction de log10(N) : euler en rouge, pointmilieu en vert, RK4 en bleu")
//e
r1=reglin(log10(N),err_euler)
r2=reglin(log10(N),err_pm)
r3=reglin(log10(N),err_RK4)





















