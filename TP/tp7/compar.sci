function [TV,TE] = compar(a,t0,T)
    N = [10,100,1000,10000]
    TV = zeros(4,3)
    TE = zeros(4,3)
    t = t0+T
    sol = (3/2)*exp(-t)-(1/2)*cos(t)+(1/2)*sin(t)
    for i = 1:4
        TV(i,1) =  eulerexpl(a, t0, T, N(i), f)(N(i)+1)
        TV(i,2) =  pointmilieu(a, t0, T, N(i), f)(N(i)+1)
        TV(i,3) = RK4(a, t0, T, N(i), f)(N(i)+1)
    end
    TE = sol-TV
endfunction
