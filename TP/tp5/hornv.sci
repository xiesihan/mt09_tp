function [p] = hornv(a, t, theta)
    m = size(theta)(1)
    p = zeros(m,1)
    for j=1:m
        p(j) = horn(a , t , theta(j))(1)
    end
endfunction
